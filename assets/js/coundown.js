let hour = 6;
let minute = 5;
let second = 5;
let count;

function start() {
    if (hour == null) {
        hour = parseInt(document.getElementById("hours").value)
        minute = parseInt(document.getElementById("minutes").value)
        second = parseInt(document.getElementById("seconds").value)
    }
    if (second == -1) {
        minute--;
        second = 59;
    }
    if (minute == -1) {
        hour--;
        minute = 59;
    }
    if (hour == -1) {
        alert("hết giờ")
        clearTimeout(count)
        return false;
    }
    count = setTimeout(function () {
        second--;
        start();
    }, 1000);
    document.getElementById("hours").innerHTML = hour;
    document.getElementById("minutes").innerHTML = minute;
    document.getElementById("seconds").innerHTML = second;
}

function stop() {
    clearTimeout(count)
}
start()